
<?php   get_header(); ?>





<!-- start content of apages  -->

<main>

    <!-- start first section -->

    <h2 href="<?php echo site_url('/projects');?>" class="section-heading"> All Projects </h2>
    <section>


    <?php  
        
        $args = array('post_type'=>'project');
        $projects = new WP_Query($args);
        while ($projects->have_posts()) 
        {
            $projects->the_post(); 
            
            ?>

                <div class="card">
                    <div class="card-img">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="card image">
                        </a>
                    </div>
                    <div class="card-description">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <h3><?php echo the_title(); ?></h3>
                        </a>
                        <p>
                            <?php echo wp_trim_words(get_the_excerpt(), 30); ?>
                        </p>
                        <a class="btn-readmore" href="<?php echo get_the_permalink(); ?>">Read More</a>
                    </div>
                </div>

            <?php

        } wp_reset_query(); 
        
    ?>

    </section>



    <div class="pagination">
        <?php echo paginate_links(); ?>
    </div>

    <!-- end first section  -->




<?php   get_footer(); ?>