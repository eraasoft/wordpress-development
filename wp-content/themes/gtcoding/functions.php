<?php 



function gt_setup()
{
    wp_enqueue_style('google-fonts',"//fonts.googleapis.com/css?family=Roboto|Roboto+Condensed|Roboto+Mono&display=swap");
    wp_enqueue_style('font-awesome',"//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css");
    wp_enqueue_style('style',get_theme_file_uri('css/style.css'),NULL,microtime(),'all');
    wp_enqueue_script("main",get_theme_file_uri("/js/main.js"),NUll,microtime(),true);
}

add_action('wp_enqueue_scripts', 'gt_setup');


// adding theme support
function gt_init()
{
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('html5',
        array('comment-list','comment-form','search-form'));
}
add_action('after_setup_theme','gt_init');


//  custom projects 

function gt_custom_post_type()
{
    register_post_type('project',
        array('rewrite'=> array('slug'=>'Projects'),
            'labels' => array(
                'name' => 'Projects',
                'singular_name'=>'Project',
                'add_new_item'=>'Add New Project',
                'edit_item'=>'Edit Project'
            ),
            'menu_icon'=>'dashicons-clipboard',
            'public'=>true,
            'has-archive' =>true, 
            'supports'=>array('title','thumbnail','editor','excerpt','comments')
        )
    );
}

add_action('init','gt_custom_post_type');
