<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
    <title>GTCoding</title>
</head>

<body>



    <!-- start nav for mobile -->

    <div id="slideoute-menu">

        <ul>
            <li>
                <a href="<?php echo site_url('');?>">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url('/blog');?>">Blog</a>
            </li>
            <li>
                <a href="<?php echo site_url('/projects');?>">Projects</a>
            </li>
            <li>
                <a href="<?php echo site_url('/about');?>">About</a>
            </li>
            <li>
                <input type="text" placeholder="Search Here">
            </li>
        </ul>

    </div>

    <!-- end nav for mobile -->




    <!-- start nav big screen -->

    <nav>
        <div id="logo-img">
            <a href="#">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" alr="GTCoding">
            </a>
        </div>
        <div id="menu-icon">
            <i class="fas fa-bars"></i>
        </div>
        <ul>
            <li>
                <a class="<?php  if(is_front_page()) echo 'active' ?>" href="<?php echo site_url('');?>">Home</a>
            </li>
            <li>
                <a class="<?php  if(get_post_type() == 'post') echo 'active' ?>" href="<?php echo site_url('/blog');?>">Blog</a>
            </li>
            <li>
                <a class="<?php  if(is_page('projects')) echo 'active' ?>" href="<?php echo site_url('/projects');?>">Projects</a>
            </li>
            <li>
                <a class="<?php  if(is_page('about')) echo 'active' ?>" href="<?php echo site_url('/about');?>">About</a>
            </li>
            <li>
                <div id="search-icon">
                    <i class="fas fa-search"></i>
                </div>
            </li>
        </ul>
    </nav>

    <div id="search-box">
        <input type="text" placeholder="Search Here">
    </div>

    <!-- end nav -->

 