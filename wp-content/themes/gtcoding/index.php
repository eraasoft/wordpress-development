
<?php   get_header(); ?>





<!-- start content of apages  -->

<main>

    <!-- start first section -->

    <h2 href="<?php echo site_url('/blog');?>" class="section-heading"> All Blogs </h2>
    <section>
        <?php  
        
          
            while (have_posts()) {
               the_post();
               
             
                
        ?>


        <div class="card">
            <div class="card-img">
                <a href="<?php echo get_the_permalink(); ?>">
                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="card image">
                </a>
            </div>
            
            <div class="card-description">
                <a href="<?php echo get_the_permalink(); ?>">
                    <h3><?php echo the_title(); ?></h3>
                </a>
            <div class="card-meta">
                <br>
                Posted By <?php echo the_author(); ?> on <?php echo the_time('F j, Y'); ?>
                <a href="#"> <?php echo  get_the_category_list(',') ?> </a>
            </div>
                <p>
                    <?php echo wp_trim_words(get_the_excerpt(), 30); ?>
                </p>
                <a class="btn-readmore" href="<?php echo get_the_permalink(); ?>">Read More</a>
            </div>
        </div>

        <?php
            } wp_reset_query(); ?>



    </section>


    <div class="pagination">
        <?php echo paginate_links(); ?>
    </div>

    <!-- end first section  -->




<?php   get_footer(); ?>