<?php get_header(); ?>
<?php while (have_posts()) {
               the_post();  ?>
   <main>
<!-- start first section -->

<h2 href="#" class="page-heading"><?php echo the_title(); ?></h2>
<section class="content">
    <div class="card">
        <div class="card-img">
            <a href="#">
                <?php if(has_post_thumbnail()){  ?>
                <img src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>"   >
                <?php } ?>
            </a>
        </div>
    </div>
</section>



<section>
    <div class="card-bout">
        <div class="card-description">
            <?php the_content(); ?>
        </div>
    </div>
</section>

<section class="comment-form">
    <?php  comment_form();
    
        $comments_number = get_comments_number();
        if($comments_number != 0)
        ?>

        <div class="comments">
            <h3>What Others Say</h3>
             <ol class="all-comments">
                 <?php 
                 $comments = get_comments(
                     array('post_id'=>$post->ID,
                     'status'=>'approve'));
                     wp_list_comments(array('per_page'=>15),$comments);

                 ?>
             </ol>
        </div>

        <?php ?>
</section>

<?php } ?>

<!-- end first section  -->





<?php get_footer(); ?>