
        </main>

        <!-- start footer -->
        <footer>
            <div id="left-footer">
                <h3>Quick Links</h3>
                <ul>
                    <li>
                        <a  href="<?php echo site_url('');?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('/blog');?>">Blog</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('/projects');?>">Projects</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url    ('/about');?>">About</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('/privacy');?>">Privacy</a>
                    </li>
                </ul>
            </div>

            <div id="right-footer">
                <h3>Follow Us On</h3>
                <ul id="social-media-footer">
                    <li>
                        <a href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fab fa-github"></i>
                        </a>
                    </li>

                </ul>

                <p>This Website Designd and Developed by SeoEra</p>
            </div>

        </footer>
        <!-- end footer -->














    <!-- end content of apages  -->

    <?php wp_footer(); ?>

</body>

</html>