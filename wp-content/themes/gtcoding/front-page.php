
<?php   get_header(); ?>


    <!-- start banner  -->

    <div id="banner">
        <h1>&lt;WordPress&lt;</h1>
        <h3>Learn Coding From Scratch</h3>
    </div>

    <!-- end banner  -->



    <!-- start content of apages  -->

    <main>

        <!-- start first section -->

        <h2 href="<?php echo site_url('/blog');?>" class="section-heading">All Blogs</h2>
        <section>
            <?php  
            
                $args = array('post_type'=>'post','posts_per_page'=>2);
                $blogPosts = new WP_Query($args);
                while ($blogPosts->have_posts()) {
                    $blogPosts->the_post(); 
                    
            ?>


            <div class="card">
                <div class="card-img">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="card image">
                    </a>
                </div>
                <div class="card-description">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <h3><?php echo the_title(); ?></h3>
                    </a>
                    <p>
                        <?php echo wp_trim_words(get_the_excerpt(), 30); ?>
                    </p>
                    <a class="btn-readmore" href="<?php echo get_the_permalink(); ?>">Read More</a>
                </div>
            </div>

            <?php
                } wp_reset_query(); ?>



        </section>

        <!-- end first section  -->



        <!-- start second section -->

        <h2 href="#" class="section-heading">All Projects</h2>
        <section>


        <?php  
            
            $args = array('post_type'=>'project','posts_per_page'=>2);
            $projects = new WP_Query($args);
            while ($projects->have_posts()) 
            {
                $projects->the_post(); 
                
        ?>

            <div class="card">
                <div class="card-img">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="card image">
                    </a>
                </div>
                <div class="card-description">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <h3><?php echo the_title(); ?></h3>
                    </a>
                    <p>
                        <?php echo wp_trim_words(get_the_excerpt(), 30); ?>
                    </p>
                    <a class="btn-readmore" href="<?php echo get_the_permalink(); ?>">Read More</a>
                </div>
            </div>

        <?php
        
            } wp_reset_query(); 
            
        ?>




        </section>

        <!-- end second section  -->




        <!-- start third section -->

        <h2 href="#" class="section-heading">Source Projects</h2>
        <section id="source-section">
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy Lorem Ipsum has been the industry's standard dummy Lorem Ipsum has been the industry's standard dummy
            </p>
            <a href="#" class="btn-readmore">GitHub Profile</a>

            </div>

        </section>

        <!-- end third section  -->

<?php   get_footer(); ?>