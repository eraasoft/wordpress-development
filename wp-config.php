<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5|#W^,eAyKn?DzC9^.]+zp@`fEkJ|9H3_{/M9SZv=.I`pBK {(i8nW8)u=tFTPb.' );
define( 'SECURE_AUTH_KEY',  '7}mbt9 ~UpR@A:,n>`RMo3`LbKFO.U&VzSjoM*xE/yUBb,Tbga?4QG>lK%N=&52#' );
define( 'LOGGED_IN_KEY',    '!~nVZ:lYL3vxvhY8GsPkVdVJee^XSw&e|/$F,$,/Gc.6J2b-~-m J#3iB6$# !wr' );
define( 'NONCE_KEY',        'f%TSKfX*zz})<)ATzl+6CZ6J!M/rkcDG,VyIMld=YtP.AX^*y2RTZt[Mxjshng2l' );
define( 'AUTH_SALT',        'FD*O&N&mHZU%dfp[<]L5BIX_sJrh4=k9w8zUMDygY_CuM6UbPhsIqBPPI4ktm&sP' );
define( 'SECURE_AUTH_SALT', 'ZF}GS#Vt@EJ|6qjMPr+STc8`BUXBK[dV7U=9gK$YP4alyXIv9Bh^en{RvfO:W_Er' );
define( 'LOGGED_IN_SALT',   'QjiRDCYH[XnX*FW?3#?jV_3_]uG67w;N)/C1XB(Aj--IL/tC23rO|mC2dz-!$qI@' );
define( 'NONCE_SALT',       'Hp<)5lA[L%(S84Mw<WfpP#I>*<,n#RF<d+e3wOmj[KuB,}YSsScPjgg0(<QAU/Za' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
